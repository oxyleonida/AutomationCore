package com.capgemini.ro.automationCore.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

public class DateUtils {

	/**
	 * 
	 * @param format String describing the format
	 * @param adjustments String [] describing the adjustments
	 * @param timeZoneID
	 * @return
	 */
	public static String getAdjustedDateTimeFromCurrentDateTime(String format, String[] adjustments, String timeZoneID) {
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(timeZoneID));
		for (int i=0;i<adjustments.length / 2;i++) {
			switch (adjustments[i+adjustments.length / 2].toLowerCase().trim()) {
			case "year":
				calendar.add(Calendar.YEAR, Integer.valueOf(adjustments[i]));
				break;
			case "month":
				calendar.add(Calendar.MONTH, Integer.valueOf(adjustments[i]));
				break;
			case "day":
				calendar.add(Calendar.DATE, Integer.valueOf(adjustments[i]));
				break;
			case "hour":
				calendar.add(Calendar.HOUR, Integer.valueOf(adjustments[i]));
				break;
			case "minute":
				calendar.add(Calendar.MINUTE, Integer.valueOf(adjustments[i]));
				break;
			case "second":
				calendar.add(Calendar.SECOND, Integer.valueOf(adjustments[i]));
				break;
			case "millisecond":
			case "milisecond":
				calendar.add(Calendar.MILLISECOND, Integer.valueOf(adjustments[i]));
				break;
			}
		}
		DateFormat df = new SimpleDateFormat(format);
		df.setTimeZone(TimeZone.getTimeZone(timeZoneID));
		return df.format(calendar.getTime());
	}

	/**
	 * 
	 * @param format String describing the format
	 * @param originalDateTime string describing the Date that will be adjusted
	 * @param adjustments String [] describing the adjustments
	 * @param timeZoneID
	 * @throws ParseException
	 */
	public static String getAdjustedDateTimeFromSpecificDateTime(String format, String originalDateTime, String[] adjustments, String timeZoneID) throws ParseException{
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(timeZoneID));
		calendar.setTime(((new SimpleDateFormat(format)).parse(originalDateTime)));
		for (int i=0;i<adjustments.length / 2;i++) {
			switch (adjustments[i+adjustments.length / 2].toLowerCase().trim()) {
			case "year":
				calendar.add(Calendar.YEAR, Integer.valueOf(adjustments[i]));
				break;
			case "month":
				calendar.add(Calendar.MONTH, Integer.valueOf(adjustments[i]));
				break;
			case "day":
				calendar.add(Calendar.DATE, Integer.valueOf(adjustments[i]));
				break;
			case "hour":
				calendar.add(Calendar.HOUR, Integer.valueOf(adjustments[i]));
				break;
			case "minute":
				calendar.add(Calendar.MINUTE, Integer.valueOf(adjustments[i]));
				break;
			case "second":
				calendar.add(Calendar.SECOND, Integer.valueOf(adjustments[i]));
				break;
			case "millisecond":
				calendar.add(Calendar.MILLISECOND, Integer.valueOf(adjustments[i]));
				break;
			}
		}
		DateFormat df = new SimpleDateFormat(format);
		df.setTimeZone(TimeZone.getTimeZone(timeZoneID));
		return df.format(calendar.getTime());
	}
}
