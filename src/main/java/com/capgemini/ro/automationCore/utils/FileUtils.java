package com.capgemini.ro.automationCore.utils;


import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.FileFileFilter;

import com.capgemini.ro.automationCore.Base;
import com.capgemini.ro.automationCore.constants.Constants;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.PathMatcher;
import java.util.ArrayList;
import java.util.Collection;

/**
 * FileUtitlities class;
 * the 'createFile' method is an example what should go under utilities classes
 */

public final class FileUtils extends Base {

	private static FileUtils fileUtils;
	
	private synchronized static FileUtils getInstance() {
        if (fileUtils == null) {
        	fileUtils = new FileUtils();
        }
        return fileUtils;
    }
	
	private FileUtils() {}

    /**
     * Creates a file by given path.
     * 
     * @param fileName file name
     * @return false - if file exists; true - if file was created with success
     * @throws IOException exception if file cannot be created
     */
    public static void createFile(String fileName) throws IOException {
    	createFile(fileName,"");
    }

    public static String getSystemFileSeparator() {
        return System.getProperty(Constants.SYSTEM_FILE_SEPARATOR);
    }
    
	/**
	 * Creates a file by given path and add content to it.
	 * 
	 * @param fileName The name of the file to be created.
	 * @param content The content to be written in the file.
	 */
    public static void createFile(String fileName, String content) {
		try {
			File file = new File(fileName);
			if (file.getParentFile()!=null) {
				file.getParentFile().mkdirs();				
			}
			file.createNewFile();
			//If there is content to be written, write it
			if (!"".equals(content)) {
				FileWriter fw = new FileWriter(file.getAbsoluteFile());
				BufferedWriter bw = new BufferedWriter(fw);
				bw.write(content);
				bw.close();
			}
		} catch (IOException e) {
			getInstance().logger().error(String.format("ERROR!!! Could not create or write content '%s' in file %s", content, fileName));
		}

	}

	public static Collection<File> getFileAndDirectoryList(String path){
		return org.apache.commons.io.FileUtils.listFiles(
				new File(path),
				FileFileFilter.FILE,
				DirectoryFileFilter.DIRECTORY);
	}

	public static Collection<File> applyFilterToList(Collection<File> fileList, PathMatcher matcher){
		Collection<File> filePaths = new ArrayList<>();
		for (File file : fileList)
			if (matcher.matches(file.toPath()))
				filePaths.add(file);
		return filePaths;
	}

	/**
	 * Returns relative directory path to a file
	 *
	 * @param path to file
	 * @return relative directory path
	 */
	public static String getFolderPath(String path){
		int lastSeparatorIndex = path.lastIndexOf(File.separator);
		return path.substring(0,lastSeparatorIndex);
	}

	/**
	 * Returns name of a file
	 *
	 * @param path to file
	 * @return relative directory path
	 */
	public static String getFileName(String path){
		int lastSeparatorIndex = path.lastIndexOf(File.separator);
		return path.substring(lastSeparatorIndex+1);
	}
}
