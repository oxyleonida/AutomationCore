package com.capgemini.ro.automationCore.constants;

public class dateTimeFormat {
	/**
	 * Format - yyyy-MM-dd
	 */
	public static final String yyyy_MM_dd = "yyyy-MM-dd";

	/**
	 * Format - HH:mm:ss
	 */
	public static final String HH_mm_ss = "HH:mm:ss";

	/**
	 * Format - yyyy-MM-dd HH:mm:ss
	 */
	public static final String yyyy_MM_dd_HH_mm_ss = "yyyy-MM-dd HH:mm:ss";
}
