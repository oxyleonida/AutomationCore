package com.capgemini.ro.automationCore.constants;

public final class Constants {

    private Constants() {
    }

    /* Platform constants */
    public static final String PLATFORM_WINDOWS = "windows";
    public static final String PLATFORM_LINUX = "linux";
    public static final String PLATFORM_MAC = "mac";

    /* configuration filename */
    public static final String CONFIG_FILE_NAME = "properties/config.properties";
    
    /* data properties filename */
    public static final String DATA_PROP_FILE_NAME = "application.properties";

    /* file extensions */
    public static final String CSV_FILE_EXTENSION = ".csv";

    /* others */
    public static final String SYSTEM_FILE_SEPARATOR = "file.separator";
    public static final String LOG4J_FILE_PATH = "log.file.path";
    public static final String CSV_MODELS_PATH = "csv.models.path";
    public static final String CSV_DATAPROVIDERS_PATH = "csv.dataproviders.path";

    /* web main driver configurations */
    public static final String DEFAULT_WEB_DRIVER_CONFIG = "webdriver.config.main";

    /* screenshots configurations */
    public static final String SCREENSHOTS_SEPARATOR = "screenshots.separator";
    public static final String SCREENSHOTS_DATEFORMAT = "screenshots.dateformat";
    public static final String SCREENSHOTS_FOLDER = "screenshots.folder";
    public static final String SCREENSHOTS_FORMAT = "screenshots.format";
    
    /*BDD filter value */
    public static final String FILTER_VALUE = "filter.value";
    
    public static final String NGRAMS_FILE_NAME = "ngrams.txt";
}
