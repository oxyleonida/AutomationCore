package com.capgemini.ro.automationCore;

import org.apache.log4j.Logger;

/**
 * Abstract class that provide minimal functionalities necessary for all project classes.
 */
public abstract class Base {

    private final Logger _logger;

    public Base() {
        _logger = Logger.getLogger(this.getClass().getCanonicalName());
    }

    /**
     * Get logger.
     * 
     * @return an instance of the globally used logger configured for the calling class
     */
    public Logger logger() {
        return this._logger;
    }

}
