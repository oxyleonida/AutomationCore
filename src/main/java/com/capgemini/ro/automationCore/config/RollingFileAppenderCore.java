package com.capgemini.ro.automationCore.config;

import java.io.IOException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.RollingFileAppender;

import com.capgemini.ro.automationCore.constants.Constants;
import com.capgemini.ro.automationCore.utils.FileUtils;


public class RollingFileAppenderCore extends RollingFileAppender {

    /**
     * Create logs file, if it does not exist.
     */
    @Override
    public synchronized void setFile(String fileName, boolean append,
        boolean bufferedIO, int bufferSize) throws IOException {

        try {
            fileName = getFileName(fileName);
            FileUtils.createFile(fileName);

        } catch (Exception e) {
            e.printStackTrace();
        }

        super.setFile(fileName, append, bufferedIO, bufferSize);
    }

    /**
     * Determine proper project's log file name.
     * If project config.propreties provides log file name, use it. Otherwise, use default one.
     * @param fileName log4j file name
     * @return fileName that should be use to save project's logs
     * @throws Exception
     */
    private String getFileName(String fileName) throws Exception {
        ConfigProperties configProperties = new ConfigProperties(Constants.CONFIG_FILE_NAME).load();
        String configFileName = configProperties.getProperty(Constants.LOG4J_FILE_PATH);

        if (StringUtils.isNotEmpty(configFileName)) {
            fileName = configFileName;
        }
        return fileName;
    }

}