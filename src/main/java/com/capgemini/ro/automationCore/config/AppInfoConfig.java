package com.capgemini.ro.automationCore.config;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.apache.commons.io.FileUtils;

import com.capgemini.ro.automationCore.constants.Constants;



public class AppInfoConfig {
    
    /**
     * ConfigProperties static instance
     */
    private static ConfigProperties config;

    /**
     * Private constructor
     */
    private AppInfoConfig() {
    }
    
    @SuppressWarnings("deprecation")
	public static String[] getPropertiesByPrefix(String fileName, String prefix) {
    	List<String> lines = null;
    	try {
			lines = new CopyOnWriteArrayList<String>(Arrays.asList(FileUtils.readFileToString(new File(ClassLoader.getSystemResource(fileName.replaceAll("\\\\", "/")).toURI())).split("\n")));
		} catch (IOException | URISyntaxException e) {
			e.printStackTrace();
		}
    	for (String line : lines) {
    		if (!line.startsWith(prefix) || line.startsWith("#") || line.startsWith("\r") && !line.startsWith(".")) {
    			lines.remove(line);
    		}
    	}
    	return lines.toArray(new String[lines.size()]);
    }

    /**
     * Get property value by its key
     * @param key property key
     * @return property value that correspond to provided key
     */
    public static String getProperty(String key) {
        return AppInfoConfig.getConfigPropretiesInstance().getProperty(key, true);
    }
    
    /**
     * Get property value by its key; if property value is blank, return provided default value.
     * @param key property key
     * @param defaultValue default value
     * @return property value of the provided key; if it does not exist, <b>defaultValue</b> is returned.
     */
    public static String getProperty(String key, String defaultValue) {
        return AppInfoConfig.getConfigPropretiesInstance().getProperty(key, defaultValue);
    }
    
    /**
     * Get a property value by its key from the specified file; if property value is blank, return provided default value.
     * @param fileName		file name
     * @param key			key
     * @param defaultValue	defaultValue default value
     * @return				property value of the provided key; if it does not exist, <b>defaultValue</b> is returned.
     */
    public static String getProperty(String fileName, String key, String defaultValue) {
    	return new ConfigProperties(fileName).load().getProperty(key, defaultValue);
    }

    /**
     * Read application properties and store them into _config.
     * @return singleton instance of ConfigProperties
     */
    private synchronized static ConfigProperties getConfigPropretiesInstance() {
        if (config == null) {
            config = new ConfigProperties(Constants.DATA_PROP_FILE_NAME).load();
        }
        return config;
    }
   
      
}