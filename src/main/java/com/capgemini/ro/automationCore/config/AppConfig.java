package com.capgemini.ro.automationCore.config;



import org.apache.commons.lang.StringUtils;

import com.capgemini.ro.automationCore.constants.Constants;



/**
 * Give access to application config.properties properties
 */
public class AppConfig {

    /**
     * ConfigProperties static instance
     */
    private static ConfigProperties config;

    /**
     * Private constructor
     */
    private AppConfig() {
    }

    /**
     * Get property value by its key
     * @param key property key
     * @return property value that correspond to provided key
     */
    public static String getProperty(String key) {
        return AppConfig.getConfigPropretiesInstance().getProperty(key, false);
    }

    /**
     * Get property value by its key
     * @param key property key
     * @param ignoreError if true, does not shout '<b>ERROR!!!!11!ONEONEONE!</b>' when property key is missing
     * @return property value that correspond to provided key
     */
    public static String getProperty(String key, boolean ignoreError) {
        return AppConfig.getConfigPropretiesInstance().getProperty(key, ignoreError);
    }

    /**
     * Get property value by its key; if property value is blank, return provided default value.
     * @param key property key
     * @param defaultValue default value
     * @return property value that correspond to provided key; if value does not exist, return default one.
     */
    public static String getProperty(String key, String defaultValue) {
        String propertyValue = AppConfig.getProperty(key, true);
        if (StringUtils.isBlank(propertyValue)) {
            propertyValue = defaultValue;
        }
        return propertyValue;
    }

    /**
     * Read application properties and store them into _config.
     * @return singleton instance of ConfigProperties
     */
    private synchronized static ConfigProperties getConfigPropretiesInstance() {
        if (config == null) {
            config = new ConfigProperties(Constants.CONFIG_FILE_NAME).load();
        }
        return config;
    }
}
