package com.capgemini.ro.automationCore.config;

import java.io.IOException;

import org.apache.commons.configuration.AbstractConfiguration;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

import com.capgemini.ro.automationCore.Base;
import com.capgemini.ro.automationCore.interfaces.FileReader;



/**
 * Read configuration properties from provided file path;
 * file path should be relative to class loader;
 * Provides a method to retrieve properties value by they key.
 */
class ConfigProperties extends Base implements FileReader {

	private Configuration properties;
	private final String fileName;

	/**
	 * Constructor
	 * @param filePath relative to class loader (usual filename)
	 */
	public ConfigProperties(String filePath) {
		this.fileName = filePath;
	}

	/**
	 * Get property by provided key
	 * @param defaultValue default value
	 * @param key property key
	 * @return property value
	 */
	public String getProperty(String key, String defaultValue) {
		return this.properties.getString(key, defaultValue);
	}

	/**
	 * Get property by provided key
	 * @param key property key
	 * @return property value
	 */
	public String getProperty(String key) {
		return getProperty(key, false);
	}
	
	/**
	 * Get property by provided key
	 * @param key property key
	 * @param ignoreError ignore missing property error (if <code>true</code>, do not log error)
	 * @return property value
	 */
	public String getProperty(String key, Boolean ignoreError){
		if (!properties.containsKey(key) && !ignoreError) {
			logger().error(String.format("%s ERROR!!! Key '%s' could not be found in properties file '%s'.", this.getClass().getSimpleName(), key, fileName));
		}
		return this.properties.getString(key);
	}

	/**
	 * Load property file from provided file.
	 */
	public ConfigProperties load() {
		try {
			// Set list delimiter to none, so that apache.commons.configuration.Configuration 
			// doesn't split property values
			AbstractConfiguration.setDefaultListDelimiter('\0');
			properties = new PropertiesConfiguration(fileName);
		} catch (ConfigurationException ex) {
			logger().error("Failed to load configuration properties", ex);
			System.out.println("Failed to load configuration properties " + ex);
		}
		return this;
	}

	public FileReader load(String filename) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}
}
