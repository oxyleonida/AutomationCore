package com.capgemini.ro.automationCore.interfaces;

import java.io.IOException;

public interface FileReader {
	
	public FileReader load(String filename) throws IOException;
	
}
