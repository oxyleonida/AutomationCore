package com.capgemini.ro.automationCore.tint.xpath;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.capgemini.ro.automationCore.tint.StringUtils;

import javax.xml.namespace.NamespaceContext;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

public class XPathEvaluator {

	protected String xmlString;
	protected String encoding = StringUtils.DEFAULT_ENCODING;

	public XPathEvaluator(String xmlString) {
		this.xmlString = xmlString;
	}

	public XPathEvaluator(String xmlString, String encoding) {
		this.xmlString = xmlString;
		this.encoding = encoding;
	}

	public Object evaluate(String xpathString, QName resultType) {
		try {
			XPath xpath = XPathFactory.newInstance().newXPath();
			// xpathEvaluator.setNamespaceContext(nsContext());
			// xpath.setNamespaceContext(new UniversalNamespaceResolver(document(xmlString)));
			Document document = document(xmlString);
			xpath.setNamespaceContext(new UniversalNamespaceCache(document, false));
			Object evaluated = xpath.evaluate(xpathString, document, resultType);
			if (evaluated == null) {
				String message = String.format("Could not parse xpath %s as result type %s", xpathString, resultType.getLocalPart());
				throw new RuntimeException(message);
			}
			return evaluated;
		} catch (XPathExpressionException  | ParserConfigurationException | SAXException  | IOException e) {
			throw new RuntimeException(e);
		}
	}

	private Document document(String xml) throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		dbFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
		dbFactory.setNamespaceAware(true);
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		ByteArrayInputStream delegate = new ByteArrayInputStream(xml.getBytes(encoding));
		//BOMInputStream bomInputStream = new BOMInputStream(delegate);
		Document doc = dBuilder.parse(new InputSource(delegate));
		return doc;
	}

	private NamespaceContext nsContext() throws XMLStreamException {
		XMLInputFactory factory = XMLInputFactory.newInstance();
		XMLEventReader evtReader = factory.createXMLEventReader(stream(), encoding);
		while (evtReader.hasNext()) {
			XMLEvent event = evtReader.nextEvent();
			if (event.isStartElement()) {
				return ((StartElement) event).getNamespaceContext();
			}
		}
		throw new RuntimeException("Could not create NamespaceContext for xml: " + xmlString);
	}

	private InputStream stream() {
		try {
			InputStream stream = new ByteArrayInputStream(xmlString.getBytes(encoding));
			return  stream;
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException("Could not convert to input stream: " + xmlString + " encoding " + encoding);
		}
	}

}
