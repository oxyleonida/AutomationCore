package com.capgemini.ro.automationCore.tint;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Classes implementing this interface should be able to provide a data extraction mechanism based on XPath expressions. 
 *
 */
public interface TintData {

	/**
	 * Extract a String value located at the specified XPath.
	 * @param xpath
	 * @return
	 */
	public String getString(String xpath);

	/**
	 * Extract a Double value located at the specified XPath.
	 * @param xpath
	 * @return
	 */
	public Double getDouble(String xpath);

	/**
	 * Extract a Integer value located at the specified XPath.
	 * @param xpath
	 * @return
	 */
	public Integer getInteger(String xpath);
	
	public Boolean getBoolean(String xpath);
	
	/**
	 * Extracts a list of elements located at the specified XPath. Each element is represented as a TintData instance that can be 
	 * queried on it's own, using XPath.
	 * @param xpath
	 * @return
	 */
	public List<TintData> getList(String xpath);
	
	/**
	 * Extracts an element located at the specified XPath. The element is represented as a TintData instance that can be 
	 * queried on it's own, using XPath.
	 * @param xpath
	 * @return
	 */
	public TintData getData(String xpath);
	
	/**
	 * Extracts the wrapped value as a String.  
	 * @return
	 */
	public String asString();

	/**
	 * Extract a list of Strings located at the specified XPath.
	 * @param xpath
	 * @return
	 *
	 * @deprecated use {@link #getList(String, Class)} instead
	 */
	@Deprecated
	public List<String> getListString(String xpath);

	@Deprecated
	public List<Integer> getListInteger(String xpath);

	public <T> List<T> getList(String xpath, Class<T> clazz);

	public Date getDate(String xpath, String pattern);

}
