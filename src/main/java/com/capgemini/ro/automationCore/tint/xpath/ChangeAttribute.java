package com.capgemini.ro.automationCore.tint.xpath;

import org.w3c.dom.Node;

public class ChangeAttribute extends AbstractNodeChange {

	public ChangeAttribute(String xmlString) {
		super(xmlString);
	}
	
	public ChangeAttribute(String xmlString, String encoding) {
		super(xmlString, encoding);
	}

	@Override
	public void change(Node node, String string) {
		node.setNodeValue(string);
	}

}
