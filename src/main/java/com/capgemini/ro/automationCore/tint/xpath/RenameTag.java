package com.capgemini.ro.automationCore.tint.xpath;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

public class RenameTag extends AbstractNodeChange {

	public RenameTag(String xmlString) {
		super(xmlString);
	}
	
	public RenameTag(String xmlString, String encoding) {
		super(xmlString, encoding);
	}

	@Override
	public void change(Node node, String string) {
		Document document = node.getOwnerDocument();
		document.renameNode(node, node.getNamespaceURI(), string);
	}

}
