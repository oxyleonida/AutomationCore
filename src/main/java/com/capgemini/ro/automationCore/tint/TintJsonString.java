package com.capgemini.ro.automationCore.tint;

import static com.capgemini.ro.automationCore.tint.TintDataFactory.json;

import java.io.IOException;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.jxpath.JXPathContext;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.codehaus.jettison.json.JSONTokener;


import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * A wrapper over a JSON string. It can be used to easily extract information
 * out of the JSON using XPath.
 * 
 * <pre>
 * {@code
 * String json = "{ bicycle: { color: "red", price: 19.95  } }";
 * TintJsonString tintJsonString = new TintJsonString(json);
 * String color = tintJsonString.getString("/bicycle/@color"); // this is "red"
 * Double price = tintJsonString.getDouble("/bicycle/price"); // this is 19.95
 * </pre>
 * 
 * Usually, an XPath like "/bicycle/@color" refers to an XML tag attribute named
 * "color" and makes sense in the context of an XML. Such an XPath string is
 * supported in a JSON context by interpreting it as a child element instead of
 * an attribute.
 *
 */
public class TintJsonString implements TintData {

	private String jsonString;

	/**
	 * Uses the provided JSON string as the internal wrapped value. There is no
	 * JSON validation check done at this point. A runtime exception will be
	 * thrown when trying to extract data out of an invalid JSON, e.g. new
	 * TintJsonString(invalidJson).getString("/bicycle/price")
	 * 
	 * @param jsonString
	 */
	public TintJsonString(String jsonString) {
		this.jsonString = jsonString;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * <pre>
	 * {@code
	 * String json = "{ bicycle: { color: "red", price: 19.95  } }";
	 * TintJsonString tintJsonString = new TintJsonString(json);
	 * String color = tintJsonString.getString("/bicycle/@color"); // this is "red"
	 * </pre>
	 */
	@Override
	public String getString(String xpath) {
		return (String) value(xpath);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * <pre>
	 * {@code
	 * String json = "{ bicycle: { color: "red", price: 19.95  } }";
	 * TintJsonString tintJsonString = new TintJsonString(json);
	 * Double price = tintJsonString.getDouble("/bicycle/price"); // this is 19.95
	 * </pre>
	 */
	@Override
	public Double getDouble(String xpath) {
		return (Double) value(xpath);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * <pre>
	 * {@code
	 * String json = "{ bicycle: { color: "red", price: 10  } }";
	 * TintJsonString tintJsonString = new TintJsonString(json);
	 * Integer price = tintJsonString.getInteger("/bicycle/price"); // this is 10
	 * </pre>
	 */
	@Override
	public Integer getInteger(String xpath) {
		return (Integer) value(xpath);
	}

	@Override
	public Boolean getBoolean(String xpath) {
		return (Boolean) value(xpath);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * <pre>
	 * {@code
	 * String json = "{ bicycle: { color: "red", price: 10  } }";
	 * TintJsonString tintJsonString = new TintJsonString(json);
	 * String data = tintJsonString.asString(); // this is "{ bicycle: { color: "red", price: 10  } }"
	 * </pre>
	 */
	@Override
	public String asString() {
		return jsonString;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * <p/>
	 * 
	 * Given the following JSON:
	 * <pre>
	 *  {
	 *   store: {
	 *     book: [
	 *       { 
	 *         author: "Herman Melville",
	 *         title: "Moby Dick"
	 *       },
	 *       { 
	 *         author: "J. R. R. Tolkien",
	 *         title: "The Lord of the Rings"
	 *       }
	 *     ]
	 *   }
	 *  }
	 * </pre>
	 * 
	 * <p/>
	 * 
	 * And the following code snippet: 
	 * <pre>
	 * {@code
	 * TintJsonString tintJsonString = new TintJsonString(json);
	 * List<TintData> list = tintJsonString.getList("/store/book");
	 * }
	 * </pre>
	 * 
	 * The variable <b>list</b> contains two elements, each representing a TintData book instance that can be further queried using XPath.
	 * <p/>
	 */
	@Override
	public List<TintData> getList(String xpath) {
		List<TintData> dataList = new ArrayList<>();
		List<Map> collection = (List<Map>) value(xpath);
		for (Map map : collection) {
			dataList.add(tintJson(map));
		}
		return dataList;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * <p/>
	 * 
	 * Given the following JSON:
	 * <pre>
	 *  {
	 *   store: {
	 *     book: [
	 *       { 
	 *         author: "Herman Melville",
	 *         title: "Moby Dick"
	 *       },
	 *       { 
	 *         author: "J. R. R. Tolkien",
	 *         title: "The Lord of the Rings"
	 *       }
	 *     ]
	 *   }
	 *  }
	 * </pre>
	 * 
	 * <p/>
	 * 
	 * And the following code snippet: 
	 * <pre>
	 * {@code
	 * TintJsonString tintJsonString = new TintJsonString(json);
	 * TintData data = tintJsonString.getData("/store/book[author='Herman Melville'");
	 * }
	 * </pre>
	 * 
	 * The variable <b>data</b> contains the "Moby Dick" book as a TintData instance that can be further queried using XPath.
	 * <p/>
	 */
	@Override
	public TintData getData(String xpath) {
		Map map = (Map) value(xpath);
		return tintJson(map);
	}

	private TintJsonString tintJson(Map map) {
		try {
			StringWriter writer = new StringWriter();
			ObjectMapper objectMapper = new ObjectMapper();
			//objectMapper.configure(SerializationFeature.WRAP_ROOT_VALUE, true);
			objectMapper.writeValue(writer, map);
			return json(writer.toString());
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * <p/>
	 * 
	 * Given the following JSON:
	 * <pre>
	 *  {
	 *   store: {
	 *     book: [
	 *       { 
	 *         author: "Herman Melville",
	 *         title: "Moby Dick"
	 *       },
	 *       { 
	 *         author: "J. R. R. Tolkien",
	 *         title: "The Lord of the Rings"
	 *       }
	 *     ]
	 *   }
	 *  }
	 * </pre>
	 * 
	 * <p/>
	 * 
	 * And the following code snippet: 
	 * <pre>
	 * {@code
	 * TintJsonString tintJsonString = new TintJsonString(json);
	 * List<String> list = tintJsonString.getList("/store/book/author");
	 * }
	 * </pre>
	 * 
	 * The variable <b>list</b> contains the follwing two elements: "Herman Melville" and "J. R. R. Tolkien"
	 * <p/>
	 */
	@Override
	public List<String> getListString(String xpath) {
		return getList(xpath, String.class);
	}

	@Override
	public List<Integer> getListInteger(String xpath) {
		return getList(xpath, Integer.class);
	}

	@Override
	public <T> List<T> getList(String xpath, Class<T> clazz) {
		List<T> list = new ArrayList<>();
		Iterator iterator = iterator(xpath);
		while (iterator.hasNext()) {
			list.add((T) iterator.next());
		}
		return list;
	}
	
	

	/**
	 * Change the value located at the specified XPath.
	 * 
	 * <pre>
	 * {@code
	 * String json = "{ bicycle: { color: "red", price: 19.95  } }";
	 * TintJsonString tintJsonString = new TintJsonString(json);
	 * tintJsonString.changeValue("/bicycle/@color", "green"); // change "red" color to "green"
	 * tintJsonString.changeValue("/bicycle/price", 10.99); // change price to 10.99
	 * json = tintJsonString.asString(); // the wrapped value is now "{ bicycle: { color: "green", price: 10.99  } }" 
	 * </pre>
	 * 
	 * @param xpath
	 * @param newValue
	 */
	public void changeValue(String xpath, Object newValue) {
		JXPathContext context = context();
		context.setValue(xpath, newValue);
		Object contextBean = context.getContextBean();
		StringWriter writer = new StringWriter();
		writeObject(contextBean, writer);
		jsonString = writer.toString();
	}

	private void writeObject(Object contextBean, StringWriter writer) {
		try {
			factory().createGenerator(writer).writeObject(contextBean);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	protected Object value(String xpath) {
		try {
			return context().getValue(xpath);
		} catch (Exception e) {
			String message = String.format("Could not parse xpath %s because '%s' for data '%s'", xpath, e.getMessage(), jsonString);
			throw new RuntimeException(message, e);
		}
	}

	protected Iterator iterator(String xpath) {
		return context().iterate(xpath);
	}

	protected JXPathContext context() {
		try (JsonParser parser = factory().createParser(jsonString)) {
			JSONTokener tokener = new JSONTokener(jsonString);
			Object value = tokener.nextValue();
			if (value instanceof JSONObject) {
				Map map = parser.readValueAs(Map.class);
				return JXPathContext.newContext(map);
			} else if (value instanceof JSONArray) {
				ArrayList arrayList = parser.readValueAs(ArrayList.class);
				JXPathContext context = JXPathContext.newContext(arrayList);
				context.getVariables().declareVariable("array", arrayList);
				return context;
			} else {
				throw new RuntimeException("String data is neither JSON object, nor array");
			}
		} catch (IOException | JSONException e) {
			throw new RuntimeException(e);
		}
	}

	protected JsonFactory factory() {
		JsonFactory factory = new JsonFactory();
		factory.enable(Feature.ALLOW_UNQUOTED_FIELD_NAMES);
		factory.enable(Feature.AUTO_CLOSE_SOURCE);
		factory.setCodec(new ObjectMapper());
		return factory;
	}

	// TODO: fix copy paste code
	@Override
	public Date getDate(String xpath, String pattern) {
		String dateString = getString(xpath);
		try {
			Date date = new SimpleDateFormat(pattern).parse(dateString);
			return date;
		} catch (ParseException e) {
			String message = "Could not parse string value %s from xpath % as date, with pattern %s";
			throw new RuntimeException(String.format(message, dateString, xpath, pattern), e);
		}
	}

	public String getValueAsString (String xpath){
		Object value = null;
		try {
			value = getString(xpath);			
		} catch (Exception e) {
			try {
				value = getInteger(xpath);
			} catch (Exception e2) {
				value = getDouble(xpath);			
			}
		}
		if (value == null ){
			return null;
		}
		return value.toString();
	}

}
