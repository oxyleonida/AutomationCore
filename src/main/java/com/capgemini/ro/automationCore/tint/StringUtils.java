package com.capgemini.ro.automationCore.tint;

import static org.apache.commons.io.FileUtils.toFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.apache.commons.io.Charsets;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.BOMInputStream;



public class StringUtils {
	
	public static String DEFAULT_ENCODING = "UTF-8";
	
	@SuppressWarnings("deprecation")
	public static String fromFile(String filePath, String encoding) {
		URL resource = StringUtils.class.getResource(filePath);
		if (resource == null) {
			throw new RuntimeException("Could not find file " + filePath);
		}
		try {
			File file = toFile(resource);
			InputStream in = null;
	        try {
	            in = FileUtils.openInputStream(file);
	            BOMInputStream bomIn = new BOMInputStream(in);
	            String fileAsString = IOUtils.toString(bomIn, Charsets.toCharset(encoding));
	            return fileAsString;
	        } finally {
	            IOUtils.closeQuietly(in);
	        }
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}
	
	public static String fromFile(String filePath) {
		return fromFile(filePath, DEFAULT_ENCODING);
	}
}
