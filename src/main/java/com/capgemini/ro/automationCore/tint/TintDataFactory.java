package com.capgemini.ro.automationCore.tint;


/**
 * A helper class containing static methods that can be used to easily create TintData instances. 
 */
public class TintDataFactory {


	/**
	 * Given a response (data?) format (e.g. XML or JSON), and the actual String data content, this method creates an instance of a 
	 * TintData implementation that can be then queried using XPath.
	 *   
	 * @param data
	 * @param responseFormat
	 * @return
	 */
	public static TintData createTintData(String data, ResponseFormat responseFormat) {
		switch (responseFormat) {
		case JSON:
			return new TintJsonString(data);
		case XML:
			return new TintXmlString(data);
		default:
			throw new RuntimeException("Unknown ResponseFormat: " + responseFormat);
		}
	}

	public static TintObject object(Object obj) {
		return new TintObject(obj);
	}
	
	/**
	 * Create a TintXmlString instance that wraps the provided XML String. 
	 * No data format validation is performed on the String data content.
	 *  
	 * @param xml
	 * @return
	 */
	public static TintXmlString xml(String xml) {
		return new TintXmlString(xml);
	}

	/**
	 * Create a TintJsonString instance that wraps the provided JSON String.
	 * No data format validation is performed on the String data content.
	 * 
	 * @param json
	 * @return
	 */
	public static TintJsonString json(String json) {
		return new TintJsonString(json);
	}
}
