package com.capgemini.ro.automationCore.tint;

import static com.capgemini.ro.automationCore.tint.TintDataFactory.createTintData;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.util.EntityUtils;


public class RestResponse implements ResponseHandler<String> {

	private HttpResponse httpResponse;
	private ResponseFormat responseFormat;

	private String responseBody;

	protected RestResponse(ResponseFormat responseFormat) {
		this.responseFormat = responseFormat;
	}

	public Integer getStatusCode() {
		return httpResponse.getStatusLine().getStatusCode();
	}

	public String getReasonPhrase() {
		return httpResponse.getStatusLine().getReasonPhrase();
	}

	public String getResponseBody() {
		return responseBody;
	}


	public String getSpecificHeaders(String headerString) {
		Header[] headers = httpResponse.getHeaders(headerString);
		List<String> headerList = new ArrayList<>();
		for (Header header : headers) {
			headerList.add(header.getValue());
		}
		return StringUtils.join(headerList, ", ");
	}

	public Header[] getAllHeaders() {
		return httpResponse.getAllHeaders();
	}

	public TintData asTintData() {
		return createTintData(responseBody, responseFormat);
	}

	@Override
	public String handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
		httpResponse = response;
		HttpEntity entity = response.getEntity();
		responseBody = EntityUtils.toString(entity);
		return responseBody;
	}
}
