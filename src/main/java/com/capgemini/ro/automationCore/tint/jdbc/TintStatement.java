package com.capgemini.ro.automationCore.tint.jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.*;

public class TintStatement {

	// TODO add messages for exceptions
	@SuppressWarnings("unused")
	private int updatedRows;
	private ResultSet resultSet = null;
	private Statement statement = null;
	private Connection connection = null;
	private JdbcConnection jdbcConn = null;
	private int resultSetSize = 0;
	private Logger logger;

	public TintStatement(JdbcConnection jdbcConnection, Logger logger) {
		this.jdbcConn = jdbcConnection;
		this.logger = logger;
		init();
	}

	private void init() {
		try {
			
			connection = jdbcConn.getOpenedConnection();
			statement = connection.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					//ResultSet.CONCUR_UPDATABLE);
					ResultSet.CONCUR_READ_ONLY);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void setLogger(Logger logger){
		this.logger = logger;
	}

	public TintStatement executeQuery(String query) {
		try {
			long startTime = System.currentTimeMillis();
			if (connection.isClosed()) {
				logger.info("connection is closed. Re-opening");
				init();
			}
			resultSet = statement.executeQuery(query);
			long stopTime = System.currentTimeMillis();
			long elapsedTime = stopTime - startTime;
			logger.info("Query execution time: "+elapsedTime+" ms");
			logger.info(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return this;
	}
	
	public TintStatement executeUpdate(String query) {
		try {
			if (connection.isClosed()) {
				init();
			}
			updatedRows = statement.executeUpdate(query);
			logger.info(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return this;
	}

	public List<String> getResultSetListForColumn(String columnName)
			throws SQLException {
		List<String> resultSetList = new ArrayList<String>();

		try {
			resultSet.first();
			while (!resultSet.isAfterLast()) {
				resultSetList.add(resultSet.getString(columnName));
				resultSet.next();
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultSetList.clear();
			return resultSetList;
		}
		return resultSetList;
	}

	public String getResultForColumnByIndex(String columnName, int index) {
		try {
			return getResultSetListForColumn(columnName).get(index);
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}

	}

	public String getResultForColumn(String columnName) {
		try {
			return getResultSetListForColumn(columnName).get(0);
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	public void closeConnection() {
		try {
			if (statement != null)
				System.out.println("Closing statement.");
				statement.close();
		} catch (SQLException se) {
			se.printStackTrace();
		}
		try {
			if (connection != null){
				System.out.println("Closing connection.");
				connection.close();
			}
		} catch (SQLException se) {
			se.printStackTrace();
		}
	}
	
	public ResultSet getResultSet() {
		return resultSet;
	}

	public int getResultSize() throws SQLException {
			resultSetSize = this.resultSet.getFetchSize();
		return resultSetSize;
	}
}
