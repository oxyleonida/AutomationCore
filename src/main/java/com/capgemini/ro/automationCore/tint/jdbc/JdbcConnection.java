package com.capgemini.ro.automationCore.tint.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.log4j.Logger;


public class JdbcConnection {

	private String driverString;
	private static Connection connection;

	private String dbUrl;
	private String dbPort;
	private String dbName;
	private String username;
	private String password;
	private Logger logger;
	private String sqlhosts_path;

	public JdbcConnection(String driverString, Logger logger) {
		this.driverString = driverString;
		this.logger = logger;
	}

	
	public void setJDBCConnectionDetails(String port, String host, String dbName, String username, String password){
		this.dbUrl = host;
		this.dbPort = port;
		this.dbName = dbName;
		this.username = username;
		this.password = password;
	}

	// Initializes the JDBC driver
	public void register() {
		logger.info("Registering driver << " + driverString + " >>");
		try {
			Class.forName(driverString);
		} catch (ClassNotFoundException cnfe) {
			logger.info("Couldn't find the driver!");
			cnfe.printStackTrace();
		}

	}

	public void setLogger(Logger logger){
		this.logger = logger;
	}

	// Opens the connection
	public void openConnection() {
		logger.info("Connecting to database...");
		try {
			long startTime = System.currentTimeMillis();
			connection = DriverManager.getConnection(createSQLServerJdbcString());
			long stopTime = System.currentTimeMillis();
			long elapsedTime = stopTime - startTime;
			logger.info("Open connection time: "+elapsedTime+" ms");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public Connection getConnection() {
		if (connection != null) {
			return connection;
		} else {
			throw new NullPointerException(
					"The Connection is null. Can't get it. Initialize before using getOpenConnection()");
		}

	}

	public void closeConnection() {
		if (connection != null) {
			try {
				logger.info("Closing connection to << " + connection.getMetaData().getURL() + " >>");
				long startTime = System.currentTimeMillis();
				connection.close();
				long stopTime = System.currentTimeMillis();
				long elapsedTime = stopTime - startTime;
				logger.info("Close connection time: "+elapsedTime+" ms");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else {
			throw new NullPointerException(
					"The Connection is null. Can't get it. Initialize before using getConnection()");
		}

	}

		// Creates the jdbc string
	private String createSQLServerJdbcString() {
		StringBuilder sb = new StringBuilder();
		sb.append("jdbc:sqlserver");

		if (sqlhosts_path != null && !sqlhosts_path.isEmpty()) {
			sb.append(":DATABASE=").append(dbName).append(";SQLH_TYPE=FILE;SQLH_FILE=").append(sqlhosts_path).append(";");
		} else {
			sb.append("://").append(dbUrl).append(":").append(dbPort);//.append(";selectMethod=direct");
		}

		if (!username.isEmpty() && !password.isEmpty()) {
			sb.append(";user=").append(username).append(";password=").append(password);
		}

		return sb.toString();
	}

	public Connection getOpenedConnection(){
		register();
		openConnection();
		return getConnection();
	}

}
