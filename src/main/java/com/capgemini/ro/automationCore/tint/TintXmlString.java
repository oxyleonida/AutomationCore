package com.capgemini.ro.automationCore.tint;

import org.apache.log4j.Logger;
import org.milyn.xml.XmlUtil;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


import com.capgemini.ro.automationCore.tint.xpath.*;

import javax.xml.namespace.QName;
import javax.xml.xpath.XPathConstants;

import static com.capgemini.ro.automationCore.tint.TintDataFactory.xml;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * A wrapper over an XML string. It can be used to easily extract information out of the XML using XPath. If the encoding is not specified, a default one will be used, see
 * {@link StringUtils#DEFAULT_ENCODING}
 * 
 * <pre>
 * {@code
 * String xml = "<bicycle color=\"red\"><price>19.95</price></bicycle>"
 * TintXmlString tintXmlString = new TintXmlString(xml);
 * String color = tintXmlString.getString("/bicycle/@color"); // this is "red"
 * Double price = tintXmlString.getDouble("/bicycle/price"); // this is 19.95
 * }
 * </pre>
 */
public class TintXmlString implements TintData, XPathOperations {

	private static Logger logger = Logger.getLogger(TintXmlString.class);

	private String xmlString;
	private String encoding = StringUtils.DEFAULT_ENCODING;

	public TintXmlString() {
	}

	/**
	 * Uses the provided XML string as the internal wrapped value. There is no XML validation check done at this point. A runtime exception will be thrown when trying to extract data out of an invalid
	 * XML, e.g. new TintXmlString(invalidXml).getString("/bicycle/price")
	 * 
	 * @param xmlString
	 */
	public TintXmlString(String xmlString) {
		this.xmlString = xmlString;
	}

	/**
	 * Set the encoding used when parsing the XML.
	 * 
	 * @param encoding
	 * @return this TintXmlString
	 */
	public TintXmlString encoding(String encoding) {
		this.encoding = encoding;
		return this;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * <pre>
	 * {@code
	 * String xml = "<bicycle color=\"red\"><price>19.95</price></bicycle>"
	 * TintXmlString tintXmlString = new TintXmlString(xml);
	 * String color = tintXmlString.getString("/bicycle/@color"); // this is "red"
	 * }
	 * </pre>
	 */
	@Override
	public String getString(String xpath) {
		return (String) evaluate(xpath, XPathConstants.STRING);
	}
	
	@Override
	public Boolean getBoolean(String xpath) {
		return (Boolean) evaluate(xpath, XPathConstants.BOOLEAN);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * <pre>
	 * {@code
	 * String xml = "<bicycle color=\"red\"><price>19.95</price></bicycle>"
	 * TintXmlString tintXmlString = new TintXmlString(xml);
	 * Double price = tintXmlString.getDouble("/bicycle/price"); // this is 19.95
	 * }
	 * </pre>
	 */
	@Override
	public Double getDouble(String xpath) {
		return (Double) evaluate(xpath, XPathConstants.NUMBER);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * <pre>
	 * {@code
	 * String xml = "<bicycle color=\"red\"><price>10</price></bicycle>"
	 * TintXmlString tintXmlString = new TintXmlString(xml);
	 * Integer price = tintXmlString.getInteger("/bicycle/price"); // this is 10
	 * }
	 * </pre>
	 */
	@Override
	public Integer getInteger(String xpath) {
		return getDouble(xpath).intValue();
	}

	/**
	 * {@inheritDoc}
	 * <p/>
	 * Given the following XML:
	 * 
	 * <pre>
	 *  {@code
	 *  <?xml version="1.0" encoding="UTF-8" standalone="no"?>
	 *  <store>
	 *  	<book>
	 *  		<author>Nigel Rees</author>
	 *  		<title>Sayings of the Century</title>
	 *  	</book>
	 *  	<book>
	 *  		<author>Evelyn Waugh</author>
	 *  		<title>Sword of Honour</title>
	 *  	</book>
	 * </store> 
	 *  }
	 * </pre>
	 * 
	 * And the following code snippet:
	 * 
	 * <pre>
	 * {@code
	 * TintXmlString tintXmlString = new TintXmlString(xml);
	 * List<TintData> = tintXmlString.getList("/store/book");
	 * }
	 * </pre>
	 * 
	 * The variable <b>list</b> contains two elements, each representing a TintData book instance that can be further queried using XPath.
	 * <p/>
	 */
	// TODO: rename, because it can be confused with getList(String, Class)
	@Override
	public List<TintData> getList(String xpath) {
		List<TintData> dataList = new ArrayList<>();
		NodeList list = (NodeList) evaluate(xpath, XPathConstants.NODESET);
		for (int i = 0; i < list.getLength(); i++) {
			Node node = list.item(i);
			String serialized = XmlUtil.serialize(node, true);
			dataList.add(xml(serialized));
		}
		return dataList;
	}

	/**
	 * {@inheritDoc}
	 * <p/>
	 * Given the following XML:
	 * 
	 * <pre>
	 *  {@code
	 *  <?xml version="1.0" encoding="UTF-8" standalone="no"?>
	 *  <store>
	 *  	<book>
	 *  		<author>Nigel Rees</author>
	 *  		<title>Sayings of the Century</title>
	 *  	</book>
	 *  	<book>
	 *  		<author>Evelyn Waugh</author>
	 *  		<title>Sword of Honour</title>
	 *  	</book>
	 * </store> 
	 *  }
	 * </pre>
	 * 
	 * And the following code snippet:
	 * 
	 * <pre>
	 * {@code
	 * 	TintXmlString tintXmlString = new TintXmlString(xml);
	 * TintData data = tintXmlString.getData("/store/book[author='Herman Melville'");
	 * }
	 * </pre>
	 * 
	 * The variable <b>data</b> contains the "Moby Dick" book as a TintData instance that can be further queried using XPath.
	 * <p/>
	 */
	@Override
	public TintData getData(String xpath) {
		Node node = (Node) evaluate(xpath, XPathConstants.NODE);
		String serialized = XmlUtil.serialize(node, true);
		return xml(serialized);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * <pre>
	 * {@code
	 * String xml = "<bicycle color=\"red\"><price>19.95</price></bicycle>"
	 * TintXmlString tintXmlString = new TintXmlString(xml);
	 * String data = tintXmlString.asString(); // this is "<bicycle color=\"red\"><price>19.95</price></bicycle>"
	 * }
	 * </pre>
	 */
	@Override
	public String asString() {
		return xmlString;
	}

	private Object evaluate(String xpath, QName resultType) {
		try {
			return new XPathEvaluator(xmlString, encoding).evaluate(xpath, resultType);
		} catch (Exception e) {
			String message = String.format("Could not parse xpath %s because '%s' for data '%s'", xpath, e.getMessage(), xmlString);
			throw new RuntimeException(message, e);
		}
	}

	/**
	 * {@inheritDoc}
	 * <p/>
	 * Given the following XML:
	 * 
	 * <pre>
	 *  {@code
	 *  <?xml version="1.0" encoding="UTF-8" standalone="no"?>
	 *  <store>
	 *  	<book>
	 *  		<author>Nigel Rees</author>
	 *  		<title>Sayings of the Century</title>
	 *  	</book>
	 *  	<book>
	 *  		<author>Evelyn Waugh</author>
	 *  		<title>Sword of Honour</title>
	 *  	</book>
	 * </store> 
	 *  }
	 * </pre>
	 * 
	 * And the following code snippet:
	 * 
	 * <pre>
	 * {@code
	 * 	TintXmlString tintXmlString = new TintXmlString(xml);
	 * List<String> list = tintXmlString.getListString("/store/book/author");
	 * }
	 * </pre>
	 * 
	 * The variable <b>list</b> contains the follwing two elements: "Nigel Rees" and "Evelyn Waugh"
	 * <p/>
	 *
	 * @deprecated use {@link #getList(String, Class)}
	 */
	@Override
	@Deprecated
	public List<String> getListString(String xpath) {
		return getList(xpath, String.class);
	}
	
	@Override
	@Deprecated
	public List<Integer> getListInteger(String xpath) {
		return getList(xpath, Integer.class);
	}
	
	@Override
	public <T> List<T> getList(String xpath, Class<T> clazz) {
		List<T> list = new ArrayList<>();
		NodeList nodelList = (NodeList) evaluate(xpath, XPathConstants.NODESET);
		for (int i = 0; i < nodelList.getLength(); i++) {
			Node node = nodelList.item(i);
			T converted = convert(node.getTextContent(), clazz);
			list.add(converted);
		}
		return list;
	}
	
	@SuppressWarnings({ "deprecation", "unchecked" })
	private static <T> T convert(String textContent, Class<T> clazz) {
		if (String.class.equals(clazz)) {
			return (T) textContent;
		} else if (clazz.equals(Integer.class)) {
			return (T) new Integer(textContent);
		} else if (clazz.equals(Double.class)) {
			return (T) new Double(textContent);			
		} else {
			throw new RuntimeException(String.format("Cannot convert %s to %s", textContent, clazz.getName()));
		}
	}

	/**
	 * Change the text content of the XML tag located at the specified XPath.
	 * 
	 * <pre>
	 * {@code
	 * String xml = "<bicycle color=\"red\"><price>19.95</price></bicycle>"
	 * TintXmlString tintXmlString = new TintXmlString(xml);
	 * tintXmlString.changeText("/bicycle/price", "10.99"); // set new price as 10.99
	 * Double price = tintXmlString.getDouble("/bicycle/price"); // the price is now 10.99
	 * String newXml = tintXmlString.asString(); // the wrapped value is now "<bicycle color=\"red\"><price>10.99</price></bicycle>" 
	 * }
	 * <p/>
	 * @param xpathString
	 * @param newText
	 */
	public TintXmlString changeText(String xpathString, String newText) {
		xmlString = new ChangeTextContent(xmlString, encoding).applyChange(xpathString, newText);
		return this;
	}

	/**
	 * Change the XML tag attribute located at the specified XPath.
	 * 
	 * <pre>
	 * {@code
	 * String xml = "<bicycle color=\"red\"><price>19.95</price></bicycle>"
	 * TintXmlString tintXmlString = new TintXmlString(xml);
	 * tintXmlString.changeAttribute("/bicycle/@color", "green"); // set new color as "green"
	 * String color = tintXmlString.getString("/bicycle/@color"); // the color is now "green"
	 * String newXml = tintXmlString.asString(); // the wrapped value is now "<bicycle color=\"green\"><price>19.95</price></bicycle>" 
	 * }
	 * <p/>
	 * @param xpathString
	 * @param newAttributeName
	 */
	public TintXmlString changeAttribute(String xpathString, String newAttributeName) {
		xmlString = new ChangeAttribute(xmlString, encoding).applyChange(xpathString, newAttributeName);
		return this;
	}

	/**
	 * Rename the XML tag located at the specified XPath.
	 * 
	 * <pre>
	 * {@code
	 * String xml = "<bicycle color=\"red\"><price>19.95</price></bicycle>"
	 * TintXmlString tintXmlString = new TintXmlString(xml);
	 * tintXmlString.renameTag("/bicycle/price", "euro"); // rename tag "price" to "euro" 
	 * Double price = tintXmlString.getDouble("/bicycle/euro"); // get price using xpath with new tag name
	 * String newXml = tintXmlString.asString(); // the wrapped value is now "<bicycle color=\"red\"><euro>19.95</euro></bicycle>" 
	 * }
	 * <p/>
	 * @param xpathString
	 * @param newTagName
	 */
	public TintXmlString renameTag(String xpathString, String newTagName) {
		xmlString = new RenameTag(xmlString, encoding).applyChange(xpathString, newTagName);
		return this;
	}

	// TODO: fix copy paste code
	@Override
	public Date getDate(String xpath, String pattern) {
		String dateString = getString(xpath);
		try {
			Date date = new SimpleDateFormat(pattern).parse(dateString);
			return date;
		} catch (ParseException e) {
			String message = "Could not parse string value %s from xpath % as date, with pattern %s";
			throw new RuntimeException(String.format(message, dateString, xpath, pattern), e);
		}
	}

}
