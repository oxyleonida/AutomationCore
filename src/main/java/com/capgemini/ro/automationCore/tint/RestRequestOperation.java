package com.capgemini.ro.automationCore.tint;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpHost;
import org.apache.http.NameValuePair;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.NTCredentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;


/**
 * Represents a resource in a REST context. For example, given the URL
 * http://www.google.com:9001/api/images/21, the resource "images/21" is
 * uniquely identifying the image with the id 21.
 *
 */
public class RestRequestOperation {
	
	public static String DEFAULT_METHOD = "GET";

	private RestRequest restRequest;

	private String resource;
	
	public String getResource() {
		return resource;
	}

	public void setResource(String resource) {
		this.resource = resource;
	}

	private String method = DEFAULT_METHOD;

	private List<NameValuePair> queryParams = new ArrayList<>();
	private List<NameValuePair> postData = new ArrayList<>();

	private Map<String, String> pathParams = new HashMap<>();
	private Map<String, String> headers = new HashMap<>();
	
	private Map<String, String> files = new HashMap<>();

	private String rawContent;
	private String contentType;

	public static final String delimiter = ","; //regex query param delimiter


	public List<NameValuePair> getQueryParams() {
		return queryParams;
	}
	
	public Map<String, String> getPathParams() {
		return pathParams;
	}
	

	public void setQueryParams(List<NameValuePair> queryParams) {
		this.queryParams = queryParams;
	}

	public Map<String, String> getHeaders() {
		return headers;
	}

	public void setHeaders(Map<String, String> headers) {
		this.headers = headers;
	}

	public String getRawContent() {
		return rawContent;
	}

	protected RestRequestOperation(RestRequest restRequest) {
		this.restRequest = restRequest;
	}
	
	public RestRequestOperation contentType(String contentType) {
		this.contentType = contentType;
		return this;
	}
	
	public RestRequestOperation file(String expectedName, String fileName) {
		if (this.getClass().getResource(fileName) == null) {
			throw new RuntimeException("Could not find resource file " + fileName);
		}
		files.put(expectedName, fileName);
		return this;
	}

	/**
	 * Sets a query parameter to be sent with the HTTP request.
	 * 
	 * @param key
	 * @param values
	 * @return
	 */
	public RestRequestOperation queryParam(String key, String... values) {
		for (String value : values) {
			queryParams.add(new BasicNameValuePair(key, value));
		}
		return this;
	}

	/**
	 * Sets the HTTP resource that is wrapped by this
	 * {@code RestRequestOperation} instance
	 * 
	 * @param resource
	 * @return
	 */
	public RestRequestOperation resource(String resource) {
		this.resource = resource;
		return this;
	}

	/**
	 * Sets the HTTP method that will be performed on this REST resource.
	 * 
	 * @param method
	 * @return
	 */
	public RestRequestOperation method(String method) {
		this.method = method;
		return this;
	}


	/**
	 * Performs the actual HTTP request after all the other configurations have
	 * been applied. If a HTTP method has not been set explicitly, the default
	 * method {@link RestRequestOperation#DEFAULT_METHOD} will be used.
	 * 
	 * @return a {@link RestResponse} instance that wraps the response of the HTTP
	 *         request.
	 */
	public RestResponse response() {
		RestResponse restResponse = new RestResponse(restRequest.getFormat());
		HttpRequestBase requestBase = httpRequest();
		addHeaders(requestBase, httpHeaders());
		requestBase.setConfig(requestConfig());
		execute(requestBase, restResponse);
		return restResponse;
	}

	private void addHeaders(HttpRequestBase requestBase, Header[] headers) {
		for (Header header : headers) {
			requestBase.addHeader(header);
		}
		if (contentType != null) {
			requestBase.setHeader(HttpHeaders.CONTENT_TYPE, contentType);
		}
	}

	private Header[] httpHeaders() {
		List<Header> list = new ArrayList<>();
		for (Entry<String, String> entry : allHeaders().entrySet()) {
			list.add(new BasicHeader(entry.getKey(), entry.getValue()));
		}
		return list.toArray(new Header[0]);
	}

	@SuppressWarnings("unchecked")
	private void execute(HttpRequestBase requestBase, @SuppressWarnings("rawtypes") ResponseHandler responseHandler) {
	    CloseableHttpClient httpclient = null;
		try {
			if(restRequest.getProxy()!=null && restRequest.getProxy().getUsername()!=null){
				CredentialsProvider credsProvider = new BasicCredentialsProvider();
				if(restRequest.getProxy().getDomain() == null){
					credsProvider.setCredentials(new AuthScope(restRequest.getProxy().getHost(), restRequest.getProxy().getPort()),
				    		new UsernamePasswordCredentials(restRequest.getProxy().getUsername(), restRequest.getProxy().getPassword()));	
				} else {
					credsProvider.setCredentials(AuthScope.ANY,
			                new NTCredentials(
			                		restRequest.getProxy().getUsername(),
			                		restRequest.getProxy().getPassword(),
			                		"",
			                		restRequest.getProxy().getDomain()));
				}
			    httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();	
			} else{
				httpclient = HttpClients.custom().build();	
			}
			httpclient.execute(requestBase, responseHandler);
		}
		 catch (IOException  e) {
			throw new RuntimeException(e.getMessage(), e);
		}
		finally {
		    if(httpclient!=null){
			try {
				httpclient.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		    }
		}
	}

	private RequestConfig requestConfig() {
		if (restRequest.getProxy() == null) return null;
		HttpHost proxy = new HttpHost(restRequest.getProxy().getHost(), restRequest.getProxy().getPort());
		RequestConfig config = RequestConfig.custom().setProxy(proxy).build();
		return config;
	}

	private HttpRequestBase httpRequest() {
		if ("GET".equals(method)) {
			return new HttpGet(fullResourceUrl());
		} else if ("POST".equals(method)) {
			HttpPost httpPost = new HttpPost(fullResourceUrl());
			HttpEntity entity = getEntity();
			httpPost.setEntity(entity);
			httpPost.addHeader(HttpHeaders.CONTENT_TYPE, entity.getContentType().getValue());
			return httpPost;
		} else {
			if ("DELETE".equals(method)) {
				return new HttpDelete(fullResourceUrl()); }
			else {
				throw new RuntimeException("Method not supported: " + method);
			}
		}
		
		
		
	}

	private HttpEntity getEntity() {
		try {
			if (rawContent != null) {
				//return new StringEntity(rawContent);
				return new StringEntity(rawContent,org.apache.http.entity.ContentType.APPLICATION_JSON);
			}
			if (files.entrySet().size() != 0) {
				return multipartEntity();
			}
			return new UrlEncodedFormEntity(allPostData());
		} catch (URISyntaxException | UnsupportedEncodingException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	private HttpEntity multipartEntity() throws URISyntaxException, UnsupportedEncodingException {
		MultipartEntityBuilder builder = MultipartEntityBuilder.create();
		for (Entry<String, String> entry : files.entrySet()) {
            String fileName = entry.getValue();
            FileBody contentBody = new FileBody(new File(this.getClass().getResource(fileName).toURI()));
            builder.addPart(entry.getKey(), contentBody);
        }
		for (NameValuePair postData : allPostData()) {
			builder.addPart(postData.getName(), new StringBody(postData.getValue(), ContentType.TEXT_PLAIN));
		}
		return builder.build();
	}

	private String fullResourceUrl() {
		URIBuilder builder = new URIBuilder().setParameters(allQueryParams());
		String queryParamsString = builder.toString();
		return restRequest.getParsedEndpoint() + parsedResource() + queryParamsString;
	}

	private Map<String, String> allPathParams() {
		Map<String, String> allQueryParams = new HashMap<>();
		allQueryParams.putAll(pathParams);
		allQueryParams.putAll(restRequest.getPathParams());
		return allQueryParams;
	}
	
	private List<NameValuePair> allPostData() {
		List<NameValuePair> allPostData = new ArrayList<>();
		allPostData.addAll(postData);
		allPostData.addAll(restRequest.getPostData());
		return allPostData;
	}
	
	private Map<String, String> allHeaders() {
		Map<String, String> allQueryParams = new HashMap<>();
		allQueryParams.putAll(headers);
		allQueryParams.putAll(restRequest.getHeaders());
		return allQueryParams;
	}
	
	private List<NameValuePair> allQueryParams() {
		List<NameValuePair> allQueryParams = new ArrayList<>();
		allQueryParams.addAll(queryParams);
		allQueryParams.addAll(restRequest.getQueryParams());
		return allQueryParams;
	}

	private String parsedResource() {
		String parsed = resource;
		for (Entry<String, String> entry : allPathParams().entrySet()) {
			parsed = parsed.replaceAll("\\{" + entry.getKey() + "\\}", entry.getValue());
		}
		return parsed == null ? "" : parsed;
	}

	/**
	 * Sets a path parameter that will be used to create a proper resource name
	 * out of a parameterized resource name. For example, given the URL
	 * http://www.google.com:9001/api/images/{id}, we can make a GET request to
	 * http://www.google.com:9001/api/images/21 as follows:
	 * 
	 * <pre>
	 * {@code
	 * new RestRequest().endpoint("http://www.google.com:9001/api")
	 * 	.resource("images/{id}")
	 * 	.pathParam("id", 21)
	 * 	.get();
	 * </pre>
	 * 
	 * @param key
	 * @param value
	 * @return
	 */
	public RestRequestOperation pathParam(String key, String value) {
		pathParams.put(key, value);
		return this;
	}

	/**
	 * Sets a form data parameter to be sent with a POST request.
	 * 
	 * @param key
	 * @param values
	 * @return
	 */
	public RestRequestOperation postData(String key, String... values) {
		for (String value : values) {
			postData.add(new BasicNameValuePair(key, value));
		}
		return this;
	}

	/**
	 * Sets a HTTP header parameter to be sent with the HTTP request.
	 * 
	 * @param key
	 * @param value
	 * @return
	 */
	public RestRequestOperation header(String key, String value) {
		headers.put(key, value);
		return this;
	}

	public RestRequestOperation rawContent(String rawContent) {
		this.rawContent = rawContent;
		return this;
	}

}
