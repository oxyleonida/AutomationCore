package com.capgemini.ro.automationCore.tint.xpath;


public interface XPathOperations {
	
	/**
	 * Change the text content of the XML tag located at the specified XPath.
	 * 
	 * <pre>
	 * {@code
	 * String xml = "<bicycle color=\"red\"><price>19.95</price></bicycle>"
	 * TintXmlString tintXmlString = new TintXmlString(xml);
	 * tintXmlString.changeText("/bicycle/price", "10.99"); // set new price as 10.99
	 * Double price = tintXmlString.getDouble("/bicycle/price"); // the price is now 10.99
	 * String newXml = tintXmlString.asString(); // the wrapped value is now "<bicycle color=\"red\"><price>10.99</price></bicycle>" 
	 * }
	 * <p/>
	 * @param xpathString
	 * @param newText
	 */
	public XPathOperations changeText(String xpathString, String newText);

	/**
	 * Change the XML tag attribute located at the specified XPath.
	 * 
	 * <pre>
	 * {@code
	 * String xml = "<bicycle color=\"red\"><price>19.95</price></bicycle>"
	 * TintXmlString tintXmlString = new TintXmlString(xml);
	 * tintXmlString.changeAttribute("/bicycle/@color", "green"); // set new color as "green"
	 * String color = tintXmlString.getString("/bicycle/@color"); // the color is now "green"
	 * String newXml = tintXmlString.asString(); // the wrapped value is now "<bicycle color=\"green\"><price>19.95</price></bicycle>" 
	 * }
	 * <p/>
	 * @param xpathString
	 * @param newAttributeName
	 */
	public XPathOperations changeAttribute(String xpathString, String newAttributeName);

	/**
	 * Rename the XML tag located at the specified XPath.
	 * 
	 * <pre>
	 * {@code
	 * String xml = "<bicycle color=\"red\"><price>19.95</price></bicycle>"
	 * TintXmlString tintXmlString = new TintXmlString(xml);
	 * tintXmlString.renameTag("/bicycle/price", "euro"); // rename tag "price" to "euro" 
	 * Double price = tintXmlString.getDouble("/bicycle/euro"); // get price using xpath with new tag name
	 * String newXml = tintXmlString.asString(); // the wrapped value is now "<bicycle color=\"red\"><euro>19.95</euro></bicycle>" 
	 * }
	 * <p/>
	 * @param xpathString
	 * @param newTagName
	 */
	public XPathOperations renameTag(String xpathString, String newTagName);

}
