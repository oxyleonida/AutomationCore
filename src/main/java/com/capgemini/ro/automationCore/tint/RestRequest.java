package com.capgemini.ro.automationCore.tint;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

/**
 * Represents a REST endpoint. The endpoint address can be defined using the
 * {@link RestRequest#base}, {@link RestRequest#port} and
 * {@link RestRequest#path} methods. The REST endpoint
 * http://www.google.com:9001/api can be defined as follows:
 * 
 * <pre>
 * {@code
 * new RestRequest().base("http://www.google.com").port(9001).path("/api") 
 * }
 * </pre>
 * 
 * The REST endpoint can also be defined as a single String using the
 * {@link RestRequest#endpoint} method, as follows:
 * 
 * <pre>
 * {@code
 * new RestRequest().endpoint("http://www.google.com:9001/api") 
 * }
 * </pre>
 *
 * If the port is not specified, the default port {@link #DEFAULT_PORT} will be
 * used. If a response format is not specified explicitly, the default
 * {@link #DEFAULT_RESPONSE_FORMAT} will be used.
 *
 * TODO: rename to RestEndpoint?
 */
public class RestRequest {

	public static ResponseFormat DEFAULT_RESPONSE_FORMAT = ResponseFormat.JSON;
	public static int DEFAULT_PORT = 80;
	
	private Proxy proxy;

	private String path = "";
	private String base;
	private int port = DEFAULT_PORT;

	private String endpoint;

	private ResponseFormat format = DEFAULT_RESPONSE_FORMAT;

	private List<NameValuePair> queryParams = new ArrayList<>();
	private List<NameValuePair> postData = new ArrayList<>();

	private Map<String, String> pathParams = new HashMap<>();
	private Map<String, String> headers = new HashMap<>();



	/**
	 * Set the base of the REST endpoint, e.g. for
	 * http://www.google.com:9001/api:
	 * 
	 * <pre>
	 * {@code
	 * new RestRequest().base("http://www.google.com") 
	 * }
	 * </pre>
	 * 
	 * @param base
	 * @return
	 */
	public RestRequest base(String base) {
		this.base = base;
		return this;
	}

	/**
	 * Set the path of the REST endpoint, e.g. for
	 * http://www.google.com:9001/api:
	 * 
	 * <pre>
	 * {@code
	 * new RestRequest().path("/api") 
	 * }
	 * </pre>
	 * 
	 * @param path
	 * @return
	 */
	public RestRequest path(String path) {
		this.path = path;
		return this;
	}

	/**
	 * Set the response format of this REST endpoint.
	 * 
	 * @param format
	 * @return
	 */
	public RestRequest format(ResponseFormat format) {
		this.format = format;
		return this;
	}

	/**
	 * Set the port of this REST endpoint, e.g. for
	 * http://www.google.com:9001/api:
	 * 
	 * <pre>
	 * {@code
	 * new RestRequest().port(9001) 
	 * }
	 * </pre>
	 * 
	 * @param port
	 * @return
	 */
	public RestRequest port(int port) {
		this.port = port;
		return this;
	}

	/**
	 * Set a global HTTP query parameter for this REST endpoint. This query
	 * parameter will be included automatically on all the
	 * {@link RestRequestOperation}s created from this {@code RestRequest}. This
	 * method is especially useful for query parameters that need to be added to
	 * all the HTTP requests sent to this REST endpoint, e.g.
	 * http://www.google.com:9001/api?authToken=dagr28txcnsswnfzqc297tsvzak
	 * 
	 * @param key
	 * @param values
	 * @return
	 */
	public RestRequest queryParam(String key, String... values) {
		for (String value : values) {
			queryParams.add(new BasicNameValuePair(key, value));
		}
		return this;
	}
	
	/**
	 * Sets a global path parameter that will be used to create a proper resource name
	 * out of a parameterized resource name. A global path parameter needs to be set before 
	 * setting the endpoint. For example, given the URL
	 * http://www.google.com:{port}/api/images/21, we can make a GET request to
	 * http://www.google.com:9001/api/images/21 as follows:
	 * 
	 * <pre>
	 * {@code
	 * new RestRequest()
	 * 	.pathParam("port", 9001)
	 * 	.endpoint("http://www.google.com:{port}/api")
	 * 	.resource("images/21")
	 * 	.get();
	 * </pre>
	 * 
	 * @param key
	 * @param value
	 * @return
	 */
	public RestRequest pathParam(String key, String value) {
		pathParams.put(key, value);
		return this;
	}

	/**
	 * Sets a global form data parameter to be sent with all subsequent POST requests of this RestRequest.
	 * 
	 * @param key
	 * @param values
	 * @return
	 */
	public RestRequest postData(String key, String... values) {
		for (String value : values) {
			postData.add(new BasicNameValuePair(key, value));
		}
		return this;
	}

	/**
	 * Sets a global HTTP header parameter to be sent with all subsequent HTTP requests of this RestRequest.
	 * 
	 * @param key
	 * @param value
	 * @return
	 */
	public RestRequest header(String key, String value) {
		headers.put(key, value);
		return this;
	}

	/**
	 * Create a REST request resource for this REST endpoint. For example, given
	 * the URL http://www.google.com:9001/api/images/21 representing the image
	 * with id 21, the {@code RestRequestOperation} can be created as follows:
	 * 
	 * <pre>
	 * {@code
	 * new RestRequest().endpoint("http://www.google.com:9001/api")
	 * 	.resource("images/21")
	 * }
	 * </pre>
	 * 
	 * @param resource
	 * @return
	 */
	public RestRequestOperation resource(String resource) {
		return new RestRequestOperation(this).resource(resource);
	}
	
	public RestRequestOperation resource() {
		return resource(null);
	}
	
	public RestRequestOperation operation() {
		return resource();
	}

	/**
	 * Alias method for {@link #resource(String)}
	 * 
	 * @param operation
	 * @return
	 */
	public RestRequestOperation operation(String operation) {
		return resource(operation);
	}

	public String getPath() {
		return path;
	}

	public String getBase() {
		return base;
	}

	public int getPort() {
		return port;
	}

	public ResponseFormat getFormat() {
		return format;
	}

	public Map<String, String> getPathParams() {
		return pathParams;
	}

	protected List<NameValuePair> getQueryParams() {
		return queryParams;
	}

	protected List<NameValuePair> getPostData() {
		return postData;
	}

	public Map<String, String> getHeaders() {
		return headers;
	}

	/**
	 * Set the endpoint where the HTTP requests will be sent.
	 * 
	 * <pre>
	 * {@code
	 * new RestRequest().endpoint("http://www.google.com:9001/api")
	 * }
	 * </pre>
	 * 
	 * @param endpoint
	 * @return
	 */
	public RestRequest endpoint(String endpoint) {
		this.endpoint = endpoint;
		try {
			URL url = new URL(parsedEndpoint(endpoint));
			this.base = url.getProtocol() + "://" + url.getHost();
			this.port = url.getPort() == -1 ? DEFAULT_PORT : url.getPort();
			this.path = url.getFile();
			return this;
		} catch (MalformedURLException e) {
			throw new RuntimeException(e);
		}
	}

	public String getEndpoint() {
		if (endpoint == null) {
			endpoint = buildEndpoint();
		}
		return endpoint;
	}

	public String getParsedEndpoint() {
		return parsedEndpoint(getEndpoint());
	}

	private String buildEndpoint() {
		try {
			if (path == null) {
				return new URL(getProtocol(), getHost(), port, "").toString();
			} else {
				return new URL(getProtocol(), getHost(), port, path).toString();
			}
        } catch (MalformedURLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
	}

	private String parsedEndpoint(String endpoint) {
		for (Entry<String, String> entry : pathParams.entrySet()) {
			endpoint = endpoint.replaceAll("\\{" + entry.getKey() + "\\}", entry.getValue());
		}
		return endpoint;
	}
	
	public RestRequest useProxy(Proxy proxy) {
		this.proxy = proxy;
		return this;
	}

	public Proxy getProxy() {
		return proxy;
	}

	public String getProtocol() {
		String[] split = base.split("://");
		String protocol = split[0];
		return protocol;
	}

	public String getHost() {
		String[] split = base.split("://");
		String host = split[1];
		return host;
	}
}
