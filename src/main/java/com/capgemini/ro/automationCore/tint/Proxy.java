package com.capgemini.ro.automationCore.tint;

public class Proxy {
	
	private String host;
	private int port;
	private String username;
	private String password;
	private String excludes;
	private String domain;
	
	private Boolean autoProxy = Boolean.FALSE;

	public String getHost() {
		return host;
	}

	public Proxy host(String host) {
		this.host = host;
		return this;
	}

	public int getPort() {
		return port;
	}

	public Proxy port(int port) {
		this.port = port;
		return this;
	}
	
	public String getUsername() {
		return username;
	}

	public Proxy username(String username) {
		this.username = username;
		return this;
	}

	public String getPassword() {
		return password;
	}

	public Proxy password(String password) {
		this.password = password;
		return this;
	}
	
	public Proxy domain(String domain){
		this.domain = domain;
		return this;
	}
	public String getExcludes() {
		return excludes;
	}

	public Proxy excludes(String excludes) {
		this.excludes = excludes;
		return this;
	}

	public Boolean getAutoProxy() {
		return autoProxy;
	}

	public String getDomain() {
		return domain;
	}

	public Proxy autoProxy(Boolean autoProxy) {
		this.autoProxy = autoProxy;
		return this;
	}

}
