package com.capgemini.ro.automationCore.tint.xpath;

import org.w3c.dom.Node;

public class ChangeTextContent extends AbstractNodeChange {

	public ChangeTextContent(String xmlString) {
		super(xmlString);
	}
	
	public ChangeTextContent(String xmlString, String encoding) {
		super(xmlString, encoding);
	}

	@Override
	public void change(Node node, String string) {
		node.setTextContent(string);
	}

}
