package com.capgemini.ro.automationCore.tint.xpath;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.capgemini.ro.automationCore.tint.StringUtils;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathConstants;
import java.io.StringWriter;

public abstract class AbstractNodeChange {
	
	private static Logger logger = LoggerFactory.getLogger(AbstractNodeChange.class);
	
	protected String xmlString;
	protected String encoding = StringUtils.DEFAULT_ENCODING;
	
	protected AbstractNodeChange(String xmlString) {
		this.xmlString = xmlString;
	}
	
	protected AbstractNodeChange(String xmlString, String encoding) {
		this.xmlString = xmlString;
		this.encoding = encoding;
	}
	
	public abstract void change(Node node, String string);

	public String applyChange(String xpathString, String string) {
		NodeList nodeList = (NodeList) new XPathEvaluator(xmlString, encoding).evaluate(xpathString, XPathConstants.NODESET);
		if (nodeList.getLength() == 0) {
			logger.warn("Could not find any nodes for xpath {}", xpathString);
			return xmlString;
		}
		
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node node = nodeList.item(i);
			try {
				change(node, string);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}

		// save the result
		Document document = nodeList.item(0).getOwnerDocument();
		return documentToString(document); // TODO: could we just return the String instead of changing xmlString?
		
	}
	
	public static String documentToString(Document document) {
		try {
			Transformer xformer = TransformerFactory.newInstance().newTransformer();
			StringWriter writer = new StringWriter();
			xformer.transform(new DOMSource(document), new StreamResult(writer));
			return writer.toString();
		} catch (TransformerFactoryConfigurationError | TransformerException e) {
			throw new RuntimeException(e);
		}
	}
}
