package com.capgemini.ro.automationCore.tint;

import org.apache.commons.jxpath.JXPathContext;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by pndl on 12/30/14.
 */
public class TintObject implements TintData {

    private Object obj;

    private JXPathContext context;

    public TintObject(Object obj) {
        this.obj = obj;
        context = JXPathContext.newContext(obj);
    }

    @Override
    public String getString(String xpath) {
        return (String) context.getValue(xpath);
    }

    @Override
    public Double getDouble(String xpath) {
        return (Double) context.getValue(xpath);
    }

    @Override
    public Integer getInteger(String xpath) {
        return (Integer) context.getValue(xpath);
    }

    @Override
    public Boolean getBoolean(String xpath) {
        return (Boolean) context.getValue(xpath);
    }

    @Override
    public List<TintData> getList(String xpath) {
        return null;
    }

    @Override
    public TintData getData(String xpath) {
        return null;
    }

    @Override
    public String asString() {
        return obj.toString();
    }

    @Override
    public List<String> getListString(String xpath) {
        return null;
    }

    @Override
    public List<Integer> getListInteger(String xpath) {
        return null;
    }

    @Override
    public <T> List<T> getList(String xpath, Class<T> clazz) {
        return null;
    }

    @Override
    public Date getDate(String xpath, String pattern) {
        String dateString = (String) context.getValue(xpath);
        try {
            return new SimpleDateFormat(pattern).parse(dateString);
        } catch (ParseException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }
}
